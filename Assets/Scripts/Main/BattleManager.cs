using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    private BoardManager boardManager;
    private List<BaseCharacter> listAttackersOnBoard = new List<BaseCharacter>();
    private List<BaseCharacter> listDefendersOnBoard = new List<BaseCharacter>();
    private List<BaseCharacter> listCharactersOnBoard = new List<BaseCharacter>();
    private List<Tile> listFreeTileThisTurn = new List<Tile>();
    private List<BaseCharacter> listCharacterCheckDie = new List<BaseCharacter>();
    //private List<BaseCharacter> listDefendersAttackableThisTurn = new List<BaseCharacter>();
    private List<CharacterAction> listActionsToDoThisTurn = new List<CharacterAction>();
    private int turnIndex;
    private bool isTurnComplete = true;
    private bool isUpdateAction = false;

    public void InitData(BoardManager boardManager)
    {
        this.boardManager = boardManager;
        turnIndex = 0;
    }


    private void CreateAttackAction(BaseCharacter owner, BaseCharacter victim)
    {
        CharacterAttackAction attackAction = new CharacterAttackAction(owner, victim, turnIndex);
        owner.ActionToDo = attackAction;
        listCharacterCheckDie.Add(victim);
        listActionsToDoThisTurn.Add(attackAction);
    }

    private void CreateMoveAction(BaseCharacter owner, Tile tileToMove)
    {
        CharacterMoveAction moveAction = new CharacterMoveAction(owner, tileToMove, boardManager, turnIndex);
        owner.ActionToDo = moveAction;
        listFreeTileThisTurn.Remove(tileToMove);
        listActionsToDoThisTurn.Add(moveAction);
    }

    private void CreateIdleAction(BaseCharacter owner)
    {
        CharacterIdleAction idleAction = new CharacterIdleAction(owner, turnIndex);
        owner.ActionToDo = idleAction;
        listActionsToDoThisTurn.Add(idleAction);
    }

    private void ResetAllCharactersAction()
    {
        foreach (BaseCharacter character in listCharactersOnBoard)
        {
            character.ResetActionData();
        }
    }

    private void CreateAllDefendersAction()
    {
        foreach (BaseCharacter defender in listDefendersOnBoard)
        {
            if (defender.ActionToDo == null)
            {
                //Attack
                List<Tile> listNeighborTiles = boardManager.GetNeighborTiles(defender.CurrentTile);
                if (listNeighborTiles.Count > 0)
                {
                    //Check if can attack
                    foreach (Tile tile in listNeighborTiles)
                    {
                        if (tile.IsFreeTile() == false && tile.CharacterOnTile.CharacterType == CharacterType.Attacker)
                        {
                            BaseCharacter attacker = tile.CharacterOnTile;
                            //Add attack action for both defender + attacker
                            boardManager.FaceTarget(attacker, defender);
                            CreateAttackAction(attacker, defender);
                            boardManager.FaceTarget(defender, attacker);
                            CreateAttackAction(defender, attacker);
                            //listDefendersAttackableThisTurn.Add(defender);
                            break;
                        }
                    }
                }

                if (defender.ActionToDo == null)
                {
                    //Idle  
                    CreateIdleAction(defender);
                }
            }
        }
    }

    private void CreateAllAttackersAction()
    {
        foreach (BaseCharacter attacker in listAttackersOnBoard)
        {
            List<Tile> listNeighborTiles = boardManager.GetNeighborTiles(attacker.CurrentTile);
            //Attack
            if (attacker.ActionToDo == null)
            {
                foreach (Tile tile in listNeighborTiles)
                {
                    if (tile.IsFreeTile() == false)
                    {
                        if (tile.CharacterOnTile.CharacterType == CharacterType.Defender)
                        {
                            boardManager.FaceTarget(attacker, tile.CharacterOnTile);
                            CreateAttackAction(attacker, tile.CharacterOnTile);
                            break;
                        }
                    }
                }
            }

            //Move
            if (attacker.ActionToDo == null)
            {
                List<Tile> listFreeNeighborTiles = new List<Tile>();
                foreach (Tile neighborTile in listNeighborTiles)
                {
                    if (listFreeTileThisTurn.Contains(neighborTile))
                    {
                        listFreeNeighborTiles.Add(neighborTile);
                    }
                }

                //Debug.LogError(listFreeNeighborTiles.Count + "VS" + listNeighborTiles.Count + "vS" + listFreeTileThisTurn.Count);
                if (listFreeNeighborTiles.Count > 0)
                {
                    //Find nearest defenders (các con defender có vị trí gần nhất = nhau)
                    List<BaseCharacter> listNearestTargets = boardManager.GetListNearestTargets(attacker, listDefendersOnBoard);
                    int nearestDistance = boardManager.GetDistanceBetween2Tiles(attacker.CurrentTile, listNearestTargets[0].CurrentTile);
                    Tile tileToMove = null;
                    bool testValue = false;

                    foreach (Tile neighborTile in listFreeNeighborTiles)
                    {
                        if (attacker.CurrentTile.X == 4 && attacker.CurrentTile.Y == 5)
                        {
                            testValue = true;
                        }
                        if (tileToMove != null)
                        {
                            break;
                        }
                        foreach (BaseCharacter target in listNearestTargets)
                        {
                            int distance = boardManager.GetDistanceBetween2Tiles(neighborTile, target.CurrentTile);
                            if (distance < nearestDistance)
                            {
                                //Found tileToMove
                                tileToMove = neighborTile;
                                nearestDistance = distance;
                                boardManager.FaceTarget(attacker, target);
                                break;
                            }
                        }
                    }

                    if (tileToMove != null)
                    {
                        //Create Move Action
                        CreateMoveAction(attacker, tileToMove);
                    }
                }
            }

            //Idle 
            if (attacker.ActionToDo == null)
            {
                CreateIdleAction(attacker);
            }
        }
    }

    private void StartDoActions()
    {
        if (listActionsToDoThisTurn.Count > 0)
        {
            foreach (CharacterAction action in listActionsToDoThisTurn)
            {
                action.StartAction();
            }

            isUpdateAction = true;
        }
        else
        {
            OnCompleteAllActions();
        }
    }

    private void OnCompleteAllActions()
    {
        isUpdateAction = false;
        //Handle end turn
        RemoveCharacterDie();

        if (listAttackersOnBoard.Count == 0)
        {
            Debug.LogError("Defenders win!");
            return;
        }

        if (listDefendersOnBoard.Count == 0)
        {
            Debug.LogError("Attackers win!");
            return;
        }

        StartNextTurn();
    }

    public void RemoveCharacterDie()
    {
        for (int i = 0; i < listCharacterCheckDie.Count; i++)
        {
            BaseCharacter character = listCharacterCheckDie[i];
            if (character.IsDead())
            {
                boardManager.RemoveCharacterFromTile(character);
            }
        }
    }

    public void StartNextTurn()
    {
        //Check win lose
        turnIndex++;
        isTurnComplete = false;

        listAttackersOnBoard = boardManager.ListAllAttackersOnBoard;
        listDefendersOnBoard = boardManager.ListAllDefendersOnBoard;
        listCharactersOnBoard = boardManager.ListCharactersOnBoard;
        listFreeTileThisTurn = boardManager.GetListFreeTiles();
        listActionsToDoThisTurn = new List<CharacterAction>();
        listCharacterCheckDie = new List<BaseCharacter>();

        //Get actions to do
        ResetAllCharactersAction();
        CreateAllDefendersAction();
        CreateAllAttackersAction();

        //Do action
        StartDoActions();
    }

    void Update()
    {
        if (isUpdateAction == false)
        {
            return;
        }

        if (listActionsToDoThisTurn.Count > 0)
        {
            for (int i = 0; i < listActionsToDoThisTurn.Count; i++)
            {
                CharacterAction characterAction = listActionsToDoThisTurn[i];
                characterAction.UpdateAction(Time.deltaTime);
                if (characterAction.IsCompleted)
                {
                    listActionsToDoThisTurn.Remove(characterAction);
                    i--;
                }
            }
            return;
        }

        OnCompleteAllActions();
    }

}
