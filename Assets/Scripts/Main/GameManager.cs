using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private BoardManager boardManager;
    public BoardManager BoardManager
    {
        get
        {
            return boardManager;
        }
    }
    [SerializeField] private BattleManager battleManager;
    public BattleManager BattleManager
    {
        get
        {
            return battleManager;
        }
    }

    [Header("-------CameraZoom (maxZoom < minZoom)------")]
    [SerializeField] private float minZoomSize;
    public float MinZoomSize
    {
        get
        {
            return minZoomSize;
        }
    }
    [SerializeField] private float maxZoomSize;
    public float MaxZoomSize
    {
        get
        {
            return maxZoomSize;
        }
    }
    [Header("-------GameSpeed------")]
    [SerializeField] private float minGameSpeed;
    public float MinGameSpeed
    {
        get
        {
            return minGameSpeed;
        }
    }
    [SerializeField] private float maxGameSpeed;
    public float MaxGameSpeed
    {
        get
        {
            return maxGameSpeed;
        }
    }
    public float GameSpeed
    {
        get
        {
            return Time.timeScale;
        }
        private set
        {
            Time.timeScale = value;
        }
    }
    [Header("-------Character------")]
    [SerializeField] private float characterMoveDuration;
    public float CharacterMoveDuration
    {
        get
        {
            return characterMoveDuration;
        }
    }

    [SerializeField] private float characterAttackDuration;
    public float CharacterAttackDuration
    {
        get
        {
            return characterAttackDuration;
        }
    }

    [Header("DealDameTime: thời gian deal dame sau khi thực hiện anim attack <= Attack Duration")]
    [SerializeField] private float characterDealDameTime;
    public float CharacterDealDameTime
    {
        get
        {
            return characterDealDameTime;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        BoardManager.ResetData();
        BoardManager.CreateTileMapAndArmy();
        BattleManager.InitData(BoardManager);
        BattleManager.StartNextTurn();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetGameSpeed(float gameSpeed){
        GameSpeed = gameSpeed;
    }
}
