using Spine;
using Spine.Unity;
using UnityEngine;

public enum EAnimState
{
    None,
    Idle,
    Move,
    Attack,
    Die,
}

public class CharacterAnimController : MonoBehaviour
{
    [SerializeField] private float timeScale = 1f;
    [SerializeField] private SkeletonAnimation skeletonAnimation;
    [SerializeField] private AnimationReferenceAsset idleAnimReferenceAsset;
    [SerializeField] private AnimationReferenceAsset moveAnimReferenceAsset;
    [SerializeField] private AnimationReferenceAsset attackAnimReferenceAsset;

    //[SerializeField] private float moveAnimTime;
    private EAnimState currentAnimState;
    public EAnimState CurrentAnimState
    {
        get
        {
            return currentAnimState;
        }
        private set
        {
            currentAnimState = value;
        }
    }
    private AnimationReferenceAsset currentAnimRefAsset;

    public void Start()
    {
        CurrentAnimState = EAnimState.None;
        //SetCharacterAnimState(EAnimState.Idle);
    }


    private void SetAnimation(AnimationReferenceAsset animation, bool isLoop, float timeScale, float loopDuration = -1, System.Action OnComplete = null)
    {
        TrackEntry entry = skeletonAnimation.state.SetAnimation(0, animation, isLoop);
        entry.TimeScale = timeScale;

        if (OnComplete != null)
        {
            void CallBackComplete(TrackEntry trackEntry)
            {
                trackEntry.End -= CallBackComplete;
                OnComplete.Invoke();
            }
            entry.End += CallBackComplete;
        }

        if (loopDuration >= 0)
        {

            entry.TrackEnd = loopDuration * timeScale; //Time scale -> = Endtime = duration /timescale nên phải nhân lên bù lại
        }

    }

    /// <param name="loopDuration"> loopDuration < 0 thì thời gian loop = vô cực</param>
    /// <param name="loopCount"> loopCount < 0 thì k quy định số lần loop (thời gian loop = vô cực)</param>
    public void SetCharacterAnimState(EAnimState state, bool isLoop = true, int loopCount = -1, float loopDuration = -1f, System.Action OnComplete = null)
    {
        bool isValid = false;
        switch (state)
        {
            case EAnimState.Idle:
                {
                    currentAnimState = state;
                    currentAnimRefAsset = idleAnimReferenceAsset;
                    isValid = true;
                    break;
                }
            case EAnimState.Move:
                {
                    currentAnimState = state;
                    currentAnimRefAsset = moveAnimReferenceAsset;
                    isValid = true;
                    break;
                }
            case EAnimState.Attack:
                {
                    currentAnimState = state;
                    currentAnimRefAsset = attackAnimReferenceAsset;
                    isValid = true;
                    break;
                }
        }

        if (isValid)
        {
            float modifierTimeScale = 1f;
            if (loopCount > 0)
            {
                float loopDurationBasedOnCount = currentAnimRefAsset.Animation.Duration * loopCount;

                if (loopDuration > 0)
                {
                    modifierTimeScale = loopDurationBasedOnCount / loopDuration;
                }
                else
                {
                    loopDuration = loopDurationBasedOnCount;
                }
                //Debug.LogError(loopDurationBasedOnCount + "vs" + modifierTimeScale + "vs" + Time.timeScale);

                if (loopCount <= 1)
                {
                    isLoop = false;
                }
            }



            SetAnimation(currentAnimRefAsset, isLoop, Time.timeScale * modifierTimeScale, loopDuration, OnComplete);
        }
    }

    private void ForceStopAnim()
    {
        skeletonAnimation.state.ClearTracks();
    }
}
