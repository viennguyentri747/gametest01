using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterType
{
    Defender,
    Attacker,
}

public class BaseCharacter : MonoBehaviour
{
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private Transform modelTransform;
    public Transform ModelTransform
    {
        get
        {
            return modelTransform;
        }
    }
    [SerializeField] private CharacterAnimController animController;
    public CharacterAnimController AnimController
    {
        get
        {
            return animController;
        }
    }
    [SerializeField] private CharacterType characterType;
    public CharacterType CharacterType { get => characterType; }
    [SerializeField] private Tile currentTile;
    public Tile CurrentTile
    {
        get
        {
            return currentTile;
        }
    }

    [SerializeField] private float maxHealth;
    public float MaxHealth
    {
        get
        {
            return maxHealth;
        }
    }

    public float CurrentHealth
    {
        get
        {
            return healthBar.CurrentHealth;
        }
    }

    [SerializeField] private Canvas characterCanvas;
    public Canvas CharacterCanvas
    {
        get
        {
            return characterCanvas;
        }
    }


    public void SetCurrentTile(Tile tile)
    {
        currentTile = tile;
    }

    public void InitData()
    {
        healthBar.InitHealth(MaxHealth, MaxHealth);
    }

    public bool IsDead()
    {
        return CurrentHealth <= 0;
    }

    public void TakeDamage(float damage)
    {
        healthBar.TakeDamage(damage);
    }

    #region action
    [SerializeField] private int minRandomNumber;
    [SerializeField] private int maxRandomNumber;
    private int attackRandomNumber;
    public int AttackRandomNumber
    {
        get
        {
            if (attackRandomNumber == -1)
            {
                return Random.Range(minRandomNumber, maxRandomNumber + 1);
            }

            return attackRandomNumber;
        }
    }
    [SerializeField] private CharacterAction actionToDo;
    public CharacterAction ActionToDo
    {
        get { return actionToDo; }
        set { actionToDo = value; }
    }

    public void ResetActionData()
    {
        attackRandomNumber = -1;
        actionToDo = null;
    }
    #endregion
}
