using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttackAction : CharacterAction
{
    private BaseCharacter victim;
    public BaseCharacter Victim
    {
        get
        {
            return victim;
        }
        private set
        {
            this.victim = value;
        }
    }

    public CharacterAttackAction(BaseCharacter owner, BaseCharacter victim, int turnIndex) : base(owner, turnIndex)
    {
        this.Victim = victim;
    }

    private float currentAttackTime;
    private float attackDuration;
    private bool isAlreadyDealDame = false;
    private float dealDameTime;

    public override void StartAction()
    {
        currentAttackTime = 0f;
        attackDuration = GameManager.Instance.CharacterAttackDuration;
        isAlreadyDealDame = false;
        dealDameTime = GameManager.Instance.CharacterDealDameTime;

        //Debug.LogError("Attack" + victim.CurrentTile.X + " " + victim.CurrentTile.Y);
        Owner.AnimController.SetCharacterAnimState(EAnimState.Attack, true, 1, GameManager.Instance.CharacterAttackDuration/* , () => { OnComplete(); } */);
    }

    private float GetDamageDeal(BaseCharacter attacker, BaseCharacter target)
    {
        if ((3 + attacker.AttackRandomNumber - target.AttackRandomNumber) % 3 == 0)
        {
            return 4f;
        }

        if ((3 + attacker.AttackRandomNumber - target.AttackRandomNumber) % 3 == 1)
        {
            return 5f;
        }

        if ((3 + attacker.AttackRandomNumber - target.AttackRandomNumber) % 3 == 2)
        {
            return 3f;
        }

        return 0f;
    }

    private void DealDame()
    {
        float damage = GetDamageDeal(Owner, Victim);
        Victim.TakeDamage(damage);
        isAlreadyDealDame = true;
    }

    private void OnComplete()
    {
        if (isAlreadyDealDame == false)
        {
            DealDame();
        }

        IsCompleted = true;
    }

    public override void UpdateAction(float deltaTime)
    {
        if (IsCompleted)
        {
            return;
        }

        currentAttackTime += deltaTime;
        if (currentAttackTime > GameManager.Instance.CharacterAttackDuration)
        {
            OnComplete();
            return;
        }

        if (isAlreadyDealDame == false && currentAttackTime >= dealDameTime)
        {
            DealDame();
        }

    }
}