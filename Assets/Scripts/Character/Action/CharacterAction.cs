using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType
{
    Attack,
    Move,
    Idle,
}

public abstract class CharacterAction
{
    private int turnIndex;
    public int TurnIndex
    {
        get
        {
            return TurnIndex;
        }
        protected set
        {
            this.turnIndex = value;
        }
    }

    private BaseCharacter owner;
    public BaseCharacter Owner
    {
        get
        {
            return owner;
        }
        protected set
        {
            this.owner = value;
        }
    }

    private bool isCompleted;
    public bool IsCompleted
    {
        get
        {
            return isCompleted;
        }
        protected set
        {
            this.isCompleted = value;
        }
    }

    public CharacterAction(BaseCharacter owner, int turnIndex)
    {
        this.IsCompleted = false;
        this.Owner = owner;
        this.TurnIndex = turnIndex;
    }

    public abstract void StartAction();
    public abstract void UpdateAction(float deltaTime);
}
