using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveAction : CharacterAction
{
    private Tile tileToMove;
    public Tile TileToMove
    {
        get
        {
            return tileToMove;
        }
        private set
        {
            tileToMove = value;
        }
    }

    private BoardManager boardManager;
    public BoardManager BoardManager
    {
        get
        {
            return boardManager;
        }
        private set
        {
            boardManager = value;
        }
    }

    private float currentMoveTime;
    private float totalMoveDuration;
    private Vector3 endPos;

    public CharacterMoveAction(BaseCharacter owner, Tile tileToMove, BoardManager boardManager, int turnIndex) : base(owner, turnIndex)
    {
        this.TileToMove = tileToMove;
        this.BoardManager = boardManager;
        this.currentMoveTime = 0;
        this.totalMoveDuration = GameManager.Instance.CharacterMoveDuration;
        this.endPos = new Vector3(tileToMove.transform.position.x, tileToMove.transform.position.y, tileToMove.transform.position.z - 1);
    }

    public override void StartAction()
    {
        Owner.AnimController.SetCharacterAnimState(EAnimState.Move);
    }

    private void OnMoveComplete()
    {
        Owner.transform.position = endPos;
        IsCompleted = true;
        Owner.AnimController.SetCharacterAnimState(EAnimState.Idle);
        GameManager.Instance.BoardManager.SetCharacterOnTile(Owner, TileToMove);
    }

    public override void UpdateAction(float deltaTime)
    {
        if (IsCompleted)
        {
            return;
        }

        if (currentMoveTime <= totalMoveDuration)
        {
            currentMoveTime += deltaTime;
            Vector3 newPosition = Vector3.Lerp(Owner.CurrentTile.transform.position, endPos, currentMoveTime / totalMoveDuration);
            Owner.transform.position = new Vector3(newPosition.x, newPosition.y, Owner.transform.position.z);
            return;
        }

        OnMoveComplete();
    }
}
