using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterIdleAction : CharacterAction
{
    public CharacterIdleAction(BaseCharacter owner, int turnIndex) : base(owner, turnIndex)
    {

    }

    public override void StartAction()
    {
        if (Owner.AnimController.CurrentAnimState != EAnimState.Idle)
        {
            Owner.AnimController.SetCharacterAnimState(EAnimState.Idle);
        }

        IsCompleted = true;
    }

    public override void UpdateAction(float deltaTime)
    {
       
    }
}
