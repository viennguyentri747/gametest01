using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMovementController : MonoBehaviour
{
    private Camera targetCamera;
    [SerializeField] private float moveSpeed;
    private Vector3 previousDragPos = new Vector3(0, 0, 0);
    private Vector3 moveOffset = new Vector3(0, 0, 0);
    private bool isDragging = false;
    private bool isLockMove = false;

    void Start()
    {
        targetCamera = UIManager.Instance.MainCamera;
    }
    
    public void LockMove()
    {
        isLockMove = true;
    }

    public void UnlockMove()
    {
        isLockMove = false;
    }

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (IsPointerOverUIElement() == false)
            {
                if (!isDragging)
                {
                    isDragging = true;
                    previousDragPos = targetCamera.ScreenToViewportPoint(Input.mousePosition);
                }
            }
        }
        // else
        // {
        //     isDragging = false;
        // }

        if (isDragging)
        {
            Vector3 mouseViewportPoint = targetCamera.ScreenToViewportPoint(Input.mousePosition);
            moveOffset = mouseViewportPoint - previousDragPos;
            previousDragPos = mouseViewportPoint;

            targetCamera.transform.position -= moveOffset * moveSpeed; //Camera đi ngược hướng kéo
        }

        if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
        }
    }

    ///<summary>
    ///Cơ bản là check xem có bắn raycast (pointerEventData) trúng UI nào k, các UI đó cần tick Raycast Target mới có thể bị raycast trúng
    ///</summary>
    private bool IsPointerOverUIElement()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
