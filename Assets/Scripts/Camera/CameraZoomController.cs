using UnityEngine;
using UnityEngine.UI;

public class CameraZoomController : MonoBehaviour
{
    private Camera targetCamera;
    private float maxZoomSize;
    private float minZoomSize;
    private float zoomGap;
    [SerializeField] private Slider camZoomSlider;
    private float currentZoom;

    void Start()
    {
        targetCamera = UIManager.Instance.MainCamera;
        minZoomSize = GameManager.Instance.MinZoomSize;
        maxZoomSize = GameManager.Instance.MaxZoomSize;
        currentZoom = targetCamera.orthographicSize;
        zoomGap = maxZoomSize - minZoomSize;

        if (currentZoom > maxZoomSize || currentZoom < minZoomSize)
        {
            currentZoom = (minZoomSize + maxZoomSize) / 2f;
        }

        camZoomSlider.value = Mathf.Clamp((currentZoom - minZoomSize) / zoomGap, 0f, 1f);

        // camZoomSlider.onValueChanged.AddListener(delegate
        // {
        //     OnZoomSliderValueChange();
        // });
        //Debug.LogError(currentZoom);
    }

    ///<summary>
    ///<param name="flatValue"> value 0->1, 0 = closest zoom, 1 = farest zoom </param>
    ///</summary>
    public void UpdateZoomBySliderValue(float flatValue)
    {
        currentZoom = minZoomSize + zoomGap * Mathf.Clamp(flatValue, 0, 1f);
        targetCamera.orthographicSize = currentZoom;
    }

    // ///<summary>
    // ///<param name="flatValue"> value 0->1, 0 = closest zoom, 1 = farest zoom </param>
    // ///</summary>
    // private void UpdateZoomBySize(float zoomSize)
    // {
    //     currentZoom = Mathf.Clamp(zoomSize, maxCloseZoomSize, maxFarZoomSize);
    //     Vector3 previousPos = targetCamera.transform.position;
    //     targetCamera.orthographicSize = currentZoom;
    //     targetCamera.transform.position = previousPos;
    // }

    public void OnZoomSliderValueChange(float value)
    {
        UpdateZoomBySliderValue(value);
    }
}
