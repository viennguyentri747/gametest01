using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private int x;
    public int X
    {
        get
        {
            return x;
        }
    }
    [SerializeField] private int y;
    public int Y
    {
        get
        {
            return y;
        }
    }
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private BaseCharacter characterOnTile;
    public BaseCharacter CharacterOnTile
    {
        get
        {
            return characterOnTile;
        }
    }

    public void InitData(int x, int y)
    {
        this.x = x;
        this.y = y;
        characterOnTile = null;
    }

    public void SetCharacter(BaseCharacter character)
    {
        characterOnTile = character;
    }

    public void SetTileMaterial(Material material)
    {
        meshRenderer.material = material;
    }

    public bool IsFreeTile()
    {
        return CharacterOnTile == null;
    }
}
