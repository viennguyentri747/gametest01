using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    #region EditorData
    [SerializeField] private Tile hexPrefab;
    float hexWidth = 1.73f;
    float hexHeight = 2.0f;
    [SerializeField] private int radius;
    [SerializeField] private int distanceBetweenToTeam;
    [SerializeField] private float gapBetweenTile;
    [SerializeField] private Material attackMaterial;
    public Material AttackMaterial
    {
        get
        {
            return attackMaterial;
        }
    }
    [SerializeField] private Material defenseMaterial;
    public Material DefenseMaterial
    {
        get
        {
            return defenseMaterial;
        }
    }
    [SerializeField] private Transform tileHolder;
    [SerializeField] private BaseCharacter attackerPrefab;
    [SerializeField] private BaseCharacter defenderPrefab;
    [SerializeField] private Transform characterHolder;
    #endregion

    #region GameData
    private Tile[,] tileArr;
    private List<BaseCharacter> listCharactersOnBoard = new List<BaseCharacter>();
    public List<BaseCharacter> ListCharactersOnBoard
    {
        private set
        {
            listCharactersOnBoard = value;
        }
        get
        {
            return listCharactersOnBoard;
        }
    }
    private List<BaseCharacter> listAllAttackersOnBoard = new List<BaseCharacter>();
    public List<BaseCharacter> ListAllAttackersOnBoard
    {
        private set
        {
            listAllAttackersOnBoard = value;
        }
        get
        {
            return listAllAttackersOnBoard;
        }
    }
    private List<BaseCharacter> listAllDefendersOnBoard = new List<BaseCharacter>();
    public List<BaseCharacter> ListAllDefendersOnBoard
    {
        private set
        {
            listAllDefendersOnBoard = value;
        }
        get
        {
            return listAllDefendersOnBoard;
        }
    }

    private List<BaseCharacter> listAllCharactersDie = new List<BaseCharacter>();
    public List<BaseCharacter> ListAllCharactersDie
    {
        private set
        {
            listAllCharactersDie = value;
        }
        get
        {
            return listAllCharactersDie;
        }
    }
    #endregion


    #region public functions
    public void ResetData()
    {
        if (tileArr != null)
        {
            for (int x = tileArr.GetLowerBound(0); x <= tileArr.GetUpperBound(0); x++)
            {
                for (int y = tileArr.GetLowerBound(1); y <= tileArr.GetUpperBound(1); y++)
                {
                    if (tileArr[x, y] != null)
                    {
                        if (tileArr[x, y].CharacterOnTile)
                        {
                            DestroyImmediate(tileArr[x, y].CharacterOnTile.gameObject);
                        }
                        DestroyImmediate(tileArr[x, y].gameObject);
                    }
                }
            }
        }

        ListCharactersOnBoard = new List<BaseCharacter>();
        ListAllAttackersOnBoard = new List<BaseCharacter>();
        ListAllDefendersOnBoard = new List<BaseCharacter>();
        ListAllCharactersDie = new List<BaseCharacter>();
    }

    public void CreateTileMapAndArmy()
    {
        Vector3 center = new Vector3(0, 0, 0);
        int countInCenter = radius * 2 + 1;
        float gapXBetweenTile = (gapBetweenTile + hexWidth);
        float gapYBetweenTile = (gapBetweenTile + hexHeight) * 0.75f;
        Vector2 leftMost = new Vector2(0, 0);
        tileArr = new Tile[radius * 2 + 1, radius * 2 + 1]; //x,y

        for (int lineIndex = -radius; lineIndex <= radius; lineIndex++)
        {
            int countInLine = countInCenter - Mathf.Abs(lineIndex);
            leftMost = new Vector2(center.x - radius * gapXBetweenTile +
                0.5f * gapXBetweenTile * Mathf.Abs(lineIndex), center.y + lineIndex * gapYBetweenTile);
            CreateTile(leftMost, countInLine, gapXBetweenTile, lineIndex);
            GenerateArmy(lineIndex, countInLine);
        }

        //ResetData();
    }

    public List<Tile> GetListFreeTiles()
    {
        List<Tile> listFreeTiles = new List<Tile>();
        for (int x = tileArr.GetLowerBound(0); x <= tileArr.GetUpperBound(0); x++)
        {
            for (int y = tileArr.GetLowerBound(1); y <= tileArr.GetUpperBound(1); y++)
            {
                if (tileArr[x, y] != null)
                {
                    if (tileArr[x, y].IsFreeTile())
                    {
                        listFreeTiles.Add(tileArr[x, y]);
                    }
                }
            }
        }

        return listFreeTiles;
    }
    #endregion

    void CreateTile(Vector2 leftMost, int currentCountInLine, float gapXBetweenTile, int lineIndex) //-radius to radius
    {
        for (int j = 0; j < currentCountInLine; j++) //left to right
        {
            Vector2 tilePos = new Vector2(leftMost.x + j * gapXBetweenTile, leftMost.y);
            //Instantiate a tile
            Tile tile = Instantiate(hexPrefab) as Tile;
            tile.transform.parent = this.tileHolder;
            tile.transform.position = new Vector3(tilePos.x, tilePos.y, 0);

            int x = j;
            int y = radius - lineIndex;
            tile.InitData(x, y);

            tile.name = "Hexagon" + x + "|" + y;
            //Debug.LogError(x + "," + y);
            tileArr[x, y] = tile;
        }
    }

    public void SetCharacterOnTile(BaseCharacter character, Tile tile)
    {
        tile.SetCharacter(character);
        if (character != null)
        {
            if (character.CurrentTile != null)
            {
                character.CurrentTile.SetCharacter(null);
            }

            character.SetCurrentTile(tile);
            character.transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y, tile.transform.position.z - 1);
        }
    }

    public void RemoveCharacterFromTile(BaseCharacter character)
    {
        listAllCharactersDie.Add(character);
        Tile characterTile = character.CurrentTile;
        if (characterTile != null)
        {
            characterTile.SetCharacter(null);
            character.SetCurrentTile(null);
        }

        if (character.CharacterType == CharacterType.Defender)
        {
            ListAllDefendersOnBoard.Remove(character);
        }
        else if (character.CharacterType == CharacterType.Attacker)
        {
            listAllAttackersOnBoard.Remove(character);
        }

        ListCharactersOnBoard.Remove(character);
        character.gameObject.SetActive(false);
    }

    void CreateCharacterOnTile(Tile tile, BaseCharacter characterPrefab)
    {
        BaseCharacter character = Instantiate(characterPrefab);
        character.InitData();
        SetCharacterOnTile(character, tile);

        if (character.CharacterType == CharacterType.Attacker)
        {
            ListAllAttackersOnBoard.Add(character);
            tile.SetTileMaterial(AttackMaterial);
        }
        else if (character.CharacterType == CharacterType.Defender)
        {
            ListAllDefendersOnBoard.Add(character);
            tile.SetTileMaterial(DefenseMaterial);
        }

        character.transform.SetParent(characterHolder, true);
        ListCharactersOnBoard.Add(character);
    }

    void GenerateArmy(int lineIndex, int countInLine)
    {
        int totalAttackerAtRear = (radius - distanceBetweenToTeam) / 2;
        int gapOuterIndex = radius - totalAttackerAtRear;
        int gapInnerIndex = gapOuterIndex - distanceBetweenToTeam + 1;

        int leftGap = totalAttackerAtRear;
        int rightGap = countInLine - 1 - totalAttackerAtRear;
        int y = radius - lineIndex;
        lineIndex = Mathf.Abs(lineIndex);

        if (lineIndex < gapInnerIndex)
        {
            for (int x = 0; x < countInLine; x++)
            {
                if (x < leftGap || x > rightGap)
                {
                    //Init attack team
                    Tile tile = tileArr[x, y];
                    CreateCharacterOnTile(tile, attackerPrefab);
                    continue;
                }

                if (x > leftGap && x < rightGap)
                {
                    //Init defense team
                    Tile tile = tileArr[x, y];
                    CreateCharacterOnTile(tile, defenderPrefab);
                }
            }
        }
        else if (lineIndex <= gapOuterIndex && lineIndex >= gapInnerIndex)
        {
            for (int x = 0; x < countInLine; x++)
            {
                if (x < leftGap || x > rightGap)
                {
                    //Init attack team
                    Tile tile = tileArr[x, y];
                    CreateCharacterOnTile(tile, attackerPrefab);
                }
            }
        }
        else
        {
            for (int i = 0; i < countInLine; i++)
            {
                //Init attack team
                Tile tile = tileArr[i, y];
                CreateCharacterOnTile(tile, attackerPrefab);
            }
        }
    }

    private static int GetDistanceBetween2Tiles(int distanceY, Tile tileOnLongLine, Tile tileOnShortLine)
    {
        if (tileOnShortLine.X >= tileOnLongLine.X) //To the right
        {
            return distanceY + tileOnShortLine.X - tileOnLongLine.X;
        }
        else //To left
        {
            int distanceX = tileOnLongLine.X - tileOnShortLine.X;
            if (distanceX - distanceY <= 0)
            {
                return distanceY;
            }
            else
            {
                return distanceX;
            }
        }
    }
    #region Utils
    public int GetDistanceBetween2Tiles(Tile tileA, Tile tileB)
    {
        if (tileA.Y == tileB.Y)
        {
            return Mathf.Abs(tileA.X - tileB.X);
        }
        else
        {
            int lineIndexA = radius - tileA.Y;
            int lineIndexB = radius - tileB.Y;
            int distanceY = Mathf.Abs(tileA.Y - tileB.Y);
            if (Mathf.Abs(lineIndexA) <= Mathf.Abs(lineIndexB)) //LineA longer
            {
                return GetDistanceBetween2Tiles(distanceY, tileA, tileB);
            }
            else   //LineB longer
            {
                return GetDistanceBetween2Tiles(distanceY, tileB, tileA);
            }
        }
    }

    private bool IsValidPosition(int x, int y)
    {
        if (x < tileArr.GetLowerBound(0) || x > tileArr.GetUpperBound(0))
        {
            return false;
        }

        if (y < tileArr.GetLowerBound(1) || y > tileArr.GetUpperBound(1))
        {
            return false;
        }

        if (tileArr[x, y] == null)
        {
            return false;
        }

        return true;
    }

    public List<Tile> GetNeighborTiles(Tile tile)
    {
        List<Tile> listNeighborTiles = new List<Tile>();
        //Check left
        if (IsValidPosition(tile.X - 1, tile.Y))
        {
            listNeighborTiles.Add(tileArr[tile.X - 1, tile.Y]);
        }

        //Check right
        if (IsValidPosition(tile.X + 1, tile.Y))
        {
            listNeighborTiles.Add(tileArr[tile.X + 1, tile.Y]);
        }

        int lineIndex = radius - tile.Y;
        if (lineIndex <= 0) //Line ở dưới nhỏ hơn
        {
            //below left
            if (IsValidPosition(tile.X - 1, tile.Y + 1))
            {
                listNeighborTiles.Add(tileArr[tile.X - 1, tile.Y + 1]);
            }

            //below right
            if (IsValidPosition(tile.X, tile.Y + 1))
            {
                listNeighborTiles.Add(tileArr[tile.X, tile.Y + 1]);
            }
        }
        else //Line ở dưới to hơn
        {
            //below left
            if (IsValidPosition(tile.X, tile.Y + 1))
            {
                listNeighborTiles.Add(tileArr[tile.X, tile.Y + 1]);
            }

            //below right
            if (IsValidPosition(tile.X + 1, tile.Y + 1))
            {
                listNeighborTiles.Add(tileArr[tile.X + 1, tile.Y + 1]);
            }
        }

        if (lineIndex >= 0) //Line ở trên nhỏ hơn
        {
            //above left
            if (IsValidPosition(tile.X - 1, tile.Y - 1))
            {
                listNeighborTiles.Add(tileArr[tile.X - 1, tile.Y - 1]);
            }

            //above right
            if (IsValidPosition(tile.X, tile.Y - 1))
            {
                listNeighborTiles.Add(tileArr[tile.X, tile.Y - 1]);
            }
        }
        else //Line ở trên to hơn
        {
            //below left
            if (IsValidPosition(tile.X, tile.Y - 1))
            {
                listNeighborTiles.Add(tileArr[tile.X, tile.Y - 1]);
            }

            //below right
            if (IsValidPosition(tile.X + 1, tile.Y - 1))
            {
                listNeighborTiles.Add(tileArr[tile.X + 1, tile.Y - 1]);
            }
        }

        return listNeighborTiles;
    }

    public void FaceTarget(BaseCharacter owner, BaseCharacter target)
    {
        bool isOwnerFaceRight = owner.ModelTransform.localScale.x > 0;
        int ownerLineIndex = radius - owner.CurrentTile.Y;
        int targetLineIndex = radius - target.CurrentTile.Y;
        bool isTargetAtRight = target.CurrentTile.X - owner.CurrentTile.X + (Mathf.Abs(targetLineIndex) - Mathf.Abs(ownerLineIndex)) * 0.5f > 0;
        if (isOwnerFaceRight && !isTargetAtRight || !isOwnerFaceRight && isTargetAtRight)
        {
            owner.ModelTransform.localScale = new Vector3(owner.ModelTransform.localScale.x * -1, owner.ModelTransform.localScale.y, owner.ModelTransform.localScale.z);
        }
    }

    public List<BaseCharacter> GetListNearestTargets(BaseCharacter attacker, List<BaseCharacter> listTargets)
    {
        List<BaseCharacter> listNearestTargets = new List<BaseCharacter>() { listTargets[0] };
        int nearestDistance = GetDistanceBetween2Tiles(attacker.CurrentTile, listNearestTargets[0].CurrentTile);
        for (int i = 1; i < listTargets.Count; i++)
        {
            BaseCharacter defender = listTargets[i];
            int distance = GetDistanceBetween2Tiles(attacker.CurrentTile, defender.CurrentTile);
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                listNearestTargets.Clear();
                listNearestTargets.Add(defender);
            }
            else if (nearestDistance == distance)
            {
                listNearestTargets.Add(defender);
            }
        }

        return listNearestTargets;
    }

    #endregion
}