using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private Camera camera;
    public Camera MainCamera
    {
        get{
            return camera;
        }
    }
    [SerializeField] private CameraZoomController cameraZoomController;
    [SerializeField] private GameSpeedController gameSpeedController;
    public void OnGameSpeedSliderValueChange(float sliderValue)
    {
        gameSpeedController.OnGameSpeedSliderValueChange(sliderValue);
    }

    public void OnCameraZoomSliderValueChange(float sliderValue)
    {
        cameraZoomController.OnZoomSliderValueChange(sliderValue);
    }
}
