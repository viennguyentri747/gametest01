using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerHelper : MonoBehaviour
{
    [SerializeField] private CanvasScaler m_canvasScaler;
    void Awake()
    {
        float refWidthHeightRatio = m_canvasScaler.referenceResolution.x / m_canvasScaler.referenceResolution.y;
        float currentWidthHeightRatio = (float)Screen.width / Screen.height;
        if (currentWidthHeightRatio == refWidthHeightRatio)
        {
            m_canvasScaler.matchWidthOrHeight = 0.5f;
        }
        else if (currentWidthHeightRatio > refWidthHeightRatio)
        { //MH ngang
            m_canvasScaler.matchWidthOrHeight = 1f;
        }
        else
        {  //MH dọc
            m_canvasScaler.matchWidthOrHeight = 0f;
        }
    }

}

