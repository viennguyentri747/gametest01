using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSpeedController : MonoBehaviour
{
    [SerializeField] private Slider gameSpeedSlider;
    private float minGameSpeed;
    private float maxGameSpeed;
    private float gameSpeedGap;
    private float currentGameSpeed;
    // Start is called before the first frame update
    void Start()
    {
        minGameSpeed = GameManager.Instance.MinGameSpeed;
        maxGameSpeed = GameManager.Instance.MaxGameSpeed;
        gameSpeedGap = maxGameSpeed - minGameSpeed;
        currentGameSpeed = 1f;
        if (currentGameSpeed < minGameSpeed || currentGameSpeed > maxGameSpeed)
        {
            currentGameSpeed = (minGameSpeed + maxGameSpeed) / 2f;
        }
        gameSpeedSlider.value = Mathf.Clamp((currentGameSpeed - minGameSpeed) / gameSpeedGap, 0f, 1f);
    }

    ///<summary>
    ///<param name="sliderValue"> value 0->1, 0 = min speed, 1 = max speed</param>
    ///</summary>
    public void UpdateGameSpeedBySliderValue(float sliderValue)
    {
        currentGameSpeed = minGameSpeed + gameSpeedGap * Mathf.Clamp(sliderValue, 0, 1f);
        GameManager.Instance.SetGameSpeed(minGameSpeed + gameSpeedGap * Mathf.Clamp(sliderValue, 0, 1f));
    }

    public void OnGameSpeedSliderValueChange(float sliderValue)
    {
        UpdateGameSpeedBySliderValue(sliderValue);
    }
}
