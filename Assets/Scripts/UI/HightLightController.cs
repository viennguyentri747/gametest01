using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HightLightController : MonoBehaviour
{
    private Camera targetCamera;
    [SerializeField] private UICharacterStatComp characterStatCompPrefab;
    private UICharacterStatComp characterStatComp;
    void Start()
    {
        characterStatComp = Instantiate(characterStatCompPrefab);
        targetCamera = UIManager.Instance.MainCamera;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var hit = Physics2D.Raycast(targetCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                BaseCharacter character = hit.collider.gameObject.GetComponent<BaseCharacter>();
                if (character != null)
                {
                    characterStatComp.transform.SetParent(character.CharacterCanvas.transform, false);
                    characterStatComp.InitData(character);
                    characterStatComp.gameObject.SetActive(true);
                }
            }
        }
    }
}
