using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICharacterStatComp : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI maxHpTxt;
    [SerializeField] private TMPro.TextMeshProUGUI currentHpTxt;
    [SerializeField] private TMPro.TextMeshProUGUI randomNumberTxt;
    [SerializeField] private RectTransform panelRect;
    [SerializeField] private float yOffsetOnAttacker;
    [SerializeField] private float yOffsetOnDefender;
    private Vector3 rectOriginalLocalPosition;
    private BaseCharacter owner;

    void Start()
    {
        rectOriginalLocalPosition = panelRect.transform.localPosition;
    }
    public void InitData(BaseCharacter character)
    {
        this.owner = character;

        if (owner.CharacterType == CharacterType.Attacker)
        {
            panelRect.localPosition = new Vector3(rectOriginalLocalPosition.x, rectOriginalLocalPosition.y + yOffsetOnAttacker, rectOriginalLocalPosition.z);
        }
        else if (owner.CharacterType == CharacterType.Defender)
        {
            panelRect.localPosition = new Vector3(rectOriginalLocalPosition.x, rectOriginalLocalPosition.y + yOffsetOnDefender, rectOriginalLocalPosition.z);
        }

        maxHpTxt.text = owner.MaxHealth.ToString();
        currentHpTxt.text = owner.CurrentHealth.ToString();
        randomNumberTxt.text = owner.AttackRandomNumber.ToString();
    }

    void ResetData()
    {
        owner = null;
    }

    void Update()
    {
        // if (owner != null)
        // {
        //     randomNumberTxt.text = owner.AttackRandomNumber.ToString();
        // }
    }

    public void OnClickClose()
    {
        ResetData();
        this.gameObject.SetActive(false);
    }
}
