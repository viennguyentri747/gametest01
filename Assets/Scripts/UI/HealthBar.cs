using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider healthSlider;
    private float maxHealth;
    public float MaxHealth
    {
        get
        {
            return maxHealth;
        }
        set
        {
            maxHealth = value;
        }
    }
    private float currentHealth;
    public float CurrentHealth
    {
        get
        {
            return currentHealth;
        }
    }

    void Start()
    {

    }

    public void InitHealth(float currentHealth, float maxHealth)
    {
        this.MaxHealth = maxHealth;
        this.currentHealth = currentHealth;
        UpdateHealthSlider();
    }

    public void TakeDamage(float damage)
    {
        this.currentHealth = Mathf.Clamp(currentHealth - damage, 0f, maxHealth);
        UpdateHealthSlider();
    }

    private void UpdateHealthSlider()
    {
        if (maxHealth == 0)
        {
            maxHealth = 1;
        }
        healthSlider.value = Mathf.Clamp(currentHealth / maxHealth, 0f, 1f);
    }
}
